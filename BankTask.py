class Investment:
    def __init__(self, invest):
        self.summa = invest[0]
        self.time = invest[1]


class Bank(Investment):
    def __init__(self, invest):
        super().__init__(invest)

    def deposit(self):
        summa = self.summa
        time = self.time * 12
        percent = 0.1 / 12
        half_result = 1.0 + percent
        x = pow(half_result, time)
        result = summa * x
        return round(result)


investment = [100, 3]
investment_end = Bank(investment)
print(investment_end.deposit())
