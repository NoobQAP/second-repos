class BaseClass(object):
    # инициируем конструктор класса BaseClass
    def __init__(self, value):
        self.value = value
        print('Данное пользователем значение :{0}.'.format(self.value))


class Multiplication(BaseClass):
    def __init__(self, value):
        super().__init__(value)
        self.value *= 2
        print('Результат после умножения :{0}'.format(self.value))
# наследнуем классом Multiplication наш базовый класс


class Addition(Multiplication):
    def __init__(self, value):
        super().__init__(value)
        self.value += 5
        print('Результат после сложения :{0}'.format(self.value))
# Здесь наследуем результат из умножения что бы дальше прибавлять к нему число


class Subtraction(Addition):
    def __init__(self, value):
        super().__init__(value)
        self.value -= 123
        print('Результат после вычитания :{0}'.format(self.value))
# так же наследнием результат сложения для дальнейшего вычитания


class Main(Subtraction):
    def __init__(self, value):
        super().__init__(value)
        self.value **= 2
        print('Результат после воздведения в степень :{0}'.format(self.value))


# Здесь вызываю метод нашего последнего класса и ввожу значение
x = Main(100)
