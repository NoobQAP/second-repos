numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
new_list = [num for num in numbers if num > 0]
print(new_list)
print("NEXT".center(50, '='))


names = {'Vasili': 1, 'Dasha': 2, 'Pavel': 3, 'Egor': 4, 'Ivan': 5, 'Anton': 6, 'Andrey': 7}
days = {'monday': 1, 'tuesday': 2, 'wednesday': 3, 'thursday': 4, 'friday': 5, 'saturday': 6, 'sunday': 7}
new_dict = [{x : y for x, y in zip(days, names)}]
print(new_dict)
print("NEXT".center(50, '='))
# решил сделать через zip

a = {word: len(word) for word in ['quick', 'brown', 'fox', 'jumps', 'over', 'lazy', 'dog']}
print(a)
print("NEXT".center(50, '='))
# это как один из вариантов решения, не совсем по теме, совсем лениво))


sentence = "the quick brown fox jumps over the lazy dog"
new_sentence = sentence.split()
solution = [len(i) for i in new_sentence[0:9] if i != 'the']
print(solution)
