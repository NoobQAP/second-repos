s = 'playstation'
print(s[0])
print(s[10])
print(s[2])
print(s[-2])
print(len(s))
string = 'pythonProject'
print(string[0:8])
print(string[4:-5])
print(string[::3])
print(string[::-1])
name_string = 'my name is name'
name = 'Egor'
print(f"My name is {name}")
test_string = 'Hello world'
print(test_string.index("w"))
print(test_string.count("l"))
print(test_string.startswith("Hello"))
print(test_string.endswith("qwe"))
