from random import choice

numbers = [0, 1, 'X', 2, 3, 'D', 4, 5, 'H', 6, 7, 'Q', 8, 9, 'A']

def lotto(*args):
    number = str(choice(args))
    return number

winner_ticket = lotto(*numbers) + lotto(*numbers) + lotto(*numbers) + lotto(*numbers)

print(f'Выигрышный билет №: {winner_ticket}')