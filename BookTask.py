class Book:
    def __init__(self, book):
        self.name = book[0]
        self.author = book[1]
        self.pages = book[2]
        self.reserved = book[3]
        self.taken = book[4]

    def library(self):
        print(self.name, self.author, self.pages, self.reserved, self.taken)


class User(Book):
    def __init__(self, book):
        super().__init__(book)

    def get_book(self, book):
        if self.taken:
            print('Книгу взял другой читатель')
        else:
            self.taken = True
            print('Книгу забрали')

    def return_book(self, book):
        if self.taken:
            self.taken = False
            print('Книга возвращена')
        else:
            print('Нельзя вернуть книгу которую вы не брали')

    def __reserve_book__(self, book):
        if self.reserved:
            print('Книга зарезервирована другим читателем')
        else:
            self.reserved = True
            print('Книгу зарезервировали')


book_1 = ['Book1', 'Author', 1234, False, False]
book_2 = ['Book2', 'Author', 1234, True, True]


book1 = User(book_1)
book2 = User(book_2)
book1.library()
book2.library()
book1.get_book(book1)
book1.return_book(book1)
book2.get_book(book2)
book1.__reserve_book__(book1)
book2.__reserve_book__(book2)
