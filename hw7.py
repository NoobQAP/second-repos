def my_function():
    return [lambda x, i=i: i + x for i in range(1, 10)]


print([m(100) for m in my_function()])
print('NEXT'.center(50, '-'))


numbers = [i for i in range(1, 25)]


def filter_num(in_num):
    if(in_num % 2) == 1:
        return True
    else:
        return False


out_filter = filter(filter_num, numbers)
print("Отфильтрованный список: ", list(out_filter))
print('конец лямбды'.center(50, '-'))


def decor_task(fn):
    def wrapper(arg):
        print("ничего не понятно, но очень интересно, вроде работает?)")
        fn(arg)
    return wrapper


@decor_task
def print_sum(num):
    print(num + 1)


print_sum(999)
print('NEXT'.center(50, '-'))


def upper_string(f):
    def another_god_damn_wrapper():
        f()
        print(b)
    a = "Один и двадцать одна сотая гигаватт!!! О чём я только думал!"
    b = a.upper()
    return another_god_damn_wrapper()


def some_sentence():
    print("Один и двадцать одна сотая гигаватт!!! О чём я только думал!")


some_sentence_decorated = upper_string(some_sentence)
some_sentence()
# если честно, не знаю как по другому сделать, точнее не знаю как не выводить эту лишнюю строчку :(


def change(func):
    def wrapper_of_change(*args):
        for arg in args:
            print(f"$$$ {arg}")
            arg = reversed(arg)
        func(*args)

    return wrapper_of_change


@change
def b(*args):
    for arg in args:
        print(f"### {arg}")


if __name__ == "__main__":
    b("py", "th", "on")
