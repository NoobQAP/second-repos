a = 10
b = 5
result = a > b and a >= b
print(result)
print(a != b)
print(b == a)
print(a < b)
second_result = a >= b or a == b
print(second_result)
print(a > b)
print(a == b)
print(b > a)
first_string = 'g'
second_string = 'h'
third_result = first_string < 'k' and second_string != 'g'
print(third_result)
forth_result = first_string > 'z' or second_string == 'q'
print(forth_result)